package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener{

    // crear la estructura

    private EditText txt1;
    private EditText txt2 ;
    private   EditText txtRes;
    private Button btnSumar;
    private  Button  btnRestar;
    private  Button  btnMult;
    private  Button  btnDiv;
    private  Button  btnLimpiar;
    private  Button  btnCerrar;
    private Operaciones op = new Operaciones();


        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setEventos();



    }


    public void initComponents(){

        txt1 = (EditText) findViewById(R.id.txtNum1);
        txt2 = (EditText) findViewById(R.id.txtNum2);
        txtRes = (EditText) findViewById(R.id.txtRes);
        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnRestar = (Button) findViewById(R.id.btnResta);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDivi);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);





    }

    public void setEventos(){

        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnDiv.setOnClickListener(this);
        this.btnMult.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
            switch( view.getId()){
                case  R.id.btnSuma :
                    Toast.makeText(this,"Exito",Toast.LENGTH_LONG).show();
                    sumar();
                    break;
                case  R.id.btnResta :
                    Toast.makeText(this,"Exito",Toast.LENGTH_LONG).show();
                    resta();
                    break;
                case  R.id.btnMult :
                    Toast.makeText(this,"Exito",Toast.LENGTH_LONG).show();
                    multi();
                    break;
                case  R.id.btnDivi :
                    Toast.makeText(this,"Exito",Toast.LENGTH_LONG).show();
                    divi();
                    break;
                case R.id.btnLimpiar:
                    limp();
                    break;
                case  R.id.btnCerrar:
                    finish();
                    break;
            }
    }

    public void limp(){
            txt2.setText("");
            txt1.setText("");
            txtRes.setText("");
    }
    public void sumar(){
            op.setNum1(Float.parseFloat(txt1.getText().toString()));
            op.setNum2(Float.parseFloat(txt2.getText().toString()));
            txtRes.setText(String.valueOf(op.suma()));

    }
    public void resta(){
        op.setNum1(Float.parseFloat(txt1.getText().toString()));
        op.setNum2(Float.parseFloat(txt2.getText().toString()));
        txtRes.setText(String.valueOf(op.resta()));

    }
    public void multi(){
        op.setNum1(Float.parseFloat(txt1.getText().toString()));
        op.setNum2(Float.parseFloat(txt2.getText().toString()));
        txtRes.setText(String.valueOf(op.mult()));

    }
    public void divi(){
        op.setNum1(Float.parseFloat(txt1.getText().toString()));
        op.setNum2(Float.parseFloat(txt2.getText().toString()));
        txtRes.setText(String.valueOf(op.div()));

    }
}
